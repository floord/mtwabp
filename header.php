<!DOCTYPE html>
<html>
<head>
  <title><?php
    /*
    * Print the <title> tag based on what is being viewed.
    */
    global $page, $paged;

    wp_title( '|', true, 'right' );

    // Add the blog name.
    bloginfo( 'name' );

    // Add the blog description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
      echo " | $site_description";

    // Add a page number if necessary:
      if ( $paged >= 2 || $page >= 2 )
        echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );


      ?></title>
      <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" />
      <?php wp_head(); ?>
    </head>
    <?php

    $battr = array();

    $bback = get_post_meta($post->ID, 'body_background', true);

    if (!isset($bid)){
      $bid   = get_post_meta($post->ID, 'body_id', true);
    };

    if(is_search() || is_category() || get_post_type( $post ) == "page" || is_home() || is_single()) {
      $bid = 'index-page';
    };

    if(is_front_page()){
      $bid = 'home-page';
    };

    if ($bback != ''){
      $battr['style'] = 'background:'.$bback.';';
    };
	
    if ($bid != ''){
      $battr['id'] = $bid;
    };
    ?>

    <body <?php echo(urldecode(http_build_query($battr, ' '))); ?>>
      <body id="home-page">
        <header>
          <div class="logo"> 
            <center><img src="img/logo.png" /></center>
          <link type="text/css" rel="stylesheet" href="style.css"/>
          <title>Make This world a better place</title>
          </div>
          <br class="clear"/> 
          <form id="counter">
            <h1>we did <span style="counter">13,478</span> good deeds</h1>
            <h2>so far... so awesome!</h2>
          </form>
          <br class="clear" />
        </header>
        
