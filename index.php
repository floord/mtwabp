<!DOCTYPE html>
<?php 

get_header(); 

$user_info = get_userdata($post->post_author);
$user_meta = get_user_meta($post->post_author);
$post_categories = wp_get_post_categories( $post->ID );
$cats = array();
$cat_names = array();

foreach($post_categories as $c){
  $cat = get_category( $c );
  $cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug );
  $cat_names[] = '<strong>'.$cat->name.'</strong>';
};
?>
<br class="clear" />

<aside>
  <div class="related">
    <h3>Yesterday</h3>
    <ul>
      <li><p>things for <a href="<?php $c = get_category( $cat ); ?>">myself</a></p></li>
    </ul>
  </div>
  <br class="clear" />
  <archive>
    <h3>Archive</h3>
    <ul>
      <li><p><a href="#">March 2013</a></p></li>
    </ul>
  </archive>
  <br class="clear" />
  <nav>
    <ul>
      <li><a href="#"><h4>About us</h4></a></li>
      <li><a href="#"><h4>Guestbook</h4></a></li>
    </ul>
  </nav>
  <br class="clear" />
  <form id="search">
    <input type="text" name="search" placeholder="Search..." />
  </form>
  <br class="clear" />
  <a href="https://twitter.com/mtwabp" class="twitter-follow-button" data-show-count="false">Follow @mtwabp</a></p>
</aside>

<section id="content">
  <div class="paper">
    <img src="img/paper.png" width="600px"/>
  </div>
  <form id="good-deed">
    <input type="text" name="Name" placeholder="Name..." /><br class="clear" />
    <input type="text" name="City" placeholder="City..." /><br class="clear" />
    <input type="text" name="Good deed" placeholder="Good deed..." />
    <div id="checkboxes">
      <input type="checkbox" name="myself" value="myself">myself <br>
      <input type="checkbox" name="others" value="others">others <br>
      <input type="checkbox" name="the environment" value="the environment">the environment <br>
      <input type="checkbox" name="the community" value="the community">the community 
    </div>
    <div class="button">
      <p>contribute</p>
    </div>
  </form>
  <div id="timeline">
  <h5>What others did:</h5>
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <article> 
    <time>14 July 2012</time>
    <br class="clear"/> 
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    <div class="author">
      <author>Floor Drees</author>, <city>Vienna</city>
      <ul class="categories">
      <?php foreach($post_categories as $cat){ ?>
        <?php $c = get_category( $cat ); ?>
        <li><?php echo '<a href="'.get_category_link($c->term_id ).'" class="'.$c->slug.'">'.$c->cat_name.'</a>'; ?></li>
        <?php }; ?>
      </ul>
      <?php }; ?>
      <?php the_content(__('(Read more...)')); ?>
    </div>
  </article>
  <?php comment_form(); ?>
            </div>
    
          </article>
        <?php endwhile; else: ?>
          <p><?php _e('We could not find any posts.'); ?></p>
        <?php endif; ?>
      <br class="clear" />
    </div>
  </section>
<!-- index php -->