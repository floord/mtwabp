<!DOCTYPE html>
<?php 

get_header(); 

$user_info = get_userdata($post->post_author);
$user_meta = get_user_meta($post->post_author);
$post_categories = wp_get_post_categories( $post->ID );
$cats = array();
$cat_names = array();

foreach($post_categories as $c){
  $cat = get_category( $c );
  $cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug );
  $cat_names[] = '<strong>'.$cat->name.'</strong>';
};
?>
<br class="clear" />
<aside>
  <div class="related">
    <h3>Yesterday</h3>
    <ul>
      <li><p>things for <a href="<?php $c = get_category( $cat ); ?>">myself</a></p></li>
    </ul>
  </div>
  <br class="clear" />
  <archive>
    <h3>Archive</h3>
    <ul>
      <li><p><a href="#">March 2013</a></p></li>
    </ul>
  </archive>
  <br class="clear" />
  <nav>
    <ul>
      <li><a href="#"><h4>About us</h4></a></li>
      <li><a href="#"><h4>Guestbook</h4></a></li>
    </ul>
  </nav>
  <br class="clear" />
  <form id="search">
    <input type="text" name="search" placeholder="Search..." />
  </form>
  <br class="clear" />
  <a href="https://twitter.com/mtwabp" class="twitter-follow-button" data-show-count="false">Follow @mtwabp</a></p>
</aside>
<br class="clear" />
<div class="articles"> 
     
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <article class="blog">
      <div>
        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <?php echo get_post_meta($post->ID, 'event_date', true);?>
        <?php if(count($cats) >= 1) {?>
          <ul class="categories">
            <?php foreach($post_categories as $cat){ ?>
              <?php $c = get_category( $cat ); ?>
              <li><?php echo '<a href="'.get_category_link($c->term_id ).'" class="'.$c->slug.'">'.$c->cat_name.'</a>'; ?></li>
              <?php }; ?>
            </ul>
            <?php }; ?>
            <?php the_content(__('(Read more...)')); ?>

            <?php comment_form(); ?>

          </div>
        </article>
      <?php endwhile; else: ?>
        <p><?php _e('We could not find any posts.'); ?></p>
      <?php endif; ?>
      <br class="clear" />
    </div>
  </section>
  <!-- single php -->