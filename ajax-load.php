<?php
define('WP_USE_THEMES', false); 
require_once('../../../wp-load.php');
$posts = new WP_Query(array(
  'tag' => 'featured',
  'offset' => $_GET['offset'],
  'posts_per_page' => 3 // Change to what ever needed
    ));
  $total_posts_query = new WP_Query(array('tag' => 'featured'));
  if( $posts->have_posts() ) {
    while ($posts->have_posts()) : $posts->the_post(); 
    $bback = get_post_meta($post->ID, 'box-color', true);
    $post_categories = wp_get_post_categories( $post->ID ); 
    $category = get_category($post_categories[0]);
                  
    ?>
    <article class="<?php echo $category->slug; ?>">
      <header><?php echo $category->name; ?></header>
      <div>
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <?php  
        if ( has_post_thumbnail() ) {
          $position = get_post_meta($post->ID, 'post_thumbnail_position', true);
          if ($position == 'top') {
            the_post_thumbnail();
            the_excerpt();
          } else {
            the_excerpt();
            the_post_thumbnail();
          } 
        } else {
          the_excerpt();
        } ?>
          
      </div>
    </article>
  </div>
  <?php
  if(($posts->found_posts+((int)$_GET['offset'])) >= $total_posts_query->found_posts) { ?>
    <script type="text/javascript" charset="utf-8">
    $('#more_link').remove();
    </script>
    <?php
  };
endwhile;
wp_reset_query();
}?>
